# Welcome to the second Git exercise.
# We're in a Python file this time, but the rules are
# mostly the same.

# Let's see what's different:
first_question = "What is your favorite color?"
# In Python, values inside quotation marks like you see above
# are called Strings. Strings can contain any combination of
# letters or numbers, so they're the perfect place to write our
# answers. Go ahead and write your answer to the first question
# in the string below:
first_question_answer = "Orange"

# After you've committed and pushed, repeat the same process for
# the rest of the questions!

second_question = "What is your favorite food?"
second_question_answer = "In the words of Dolly Parton 'I never met a spud I didn't like.'"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "I'm not sure I have one, i consume almost exclusively non-fiction media"

"""
!! Reminder that you should be:
  - adding your changes after you make them
  - committing the changes you added
  - pushing your commits to your forked repository
  After answering each question!!
"""

fourth_question = "What is your favorite animal?"
fourth_question_answer = "Chetah, the only big cat you could pet and not die"

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = """Python, but not because of the hint, I find working with it to be generally more streamlined and i enjoy seeing the community around it continue
to try and push it to do everything (even when maybe they shouldn't lol)"""
